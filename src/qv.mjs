
import 'zx/globals';
import {v4 as uuid} from 'uuid';


export const prjToDoc = prj => ({
    prj: prj,
    qvw: prj.replace(/-prj$/i, '.qvw')
});


const getQvwSort = config => qvw => config?.find(el => el.name === qvw)?.sort || Infinity;
const getQvwSortFromJsonConfig = getQvwSort(fs.readJSONSync(path.join(process.cwd(), 'qv-builder.json'), {throws: false}));

export const sortDocs = docs => docs.sort((a, b) => getQvwSortFromJsonConfig(a.qvw) - getQvwSortFromJsonConfig(b.qvw));


const prefixPath = id => path => `tmp_${id}_${path}`;

export const buildQvw = async (qvw, prj, qvPath, qvwTemplatePath, buildPath) => {
    const id = uuid();
    const qvwTmp = prefixPath(id)(qvw);
    const prjTmp = prefixPath(id)(prj);

    await Promise.all([
        fs.copy(prj, prjTmp),
        fs.copyFile(qvwTemplatePath, qvwTmp)
    ]);
    await $`${qvPath} -r ${qvwTmp}`;
    await Promise.all([
        fs.move(qvwTmp, path.join(buildPath, qvw)),
        fs.remove(prjTmp)
    ]);
}
