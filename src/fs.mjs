
import 'zx/globals';


export const getPrjs = async () => {
    const prjs = await glob([
        path.join('*-prj'),
        path.join('*-PRJ')
    ], {onlyDirectories: true});
    return prjs;
}

export const getTmps = async () => {
    const tmps = await glob(['tmp_*'], {onlyFiles: false});
    return tmps;
}

export const prepareBuildFolder = async (path) => {
    await $`rm -rf ${path}`;
    await $`mkdir ${path}`;
}
