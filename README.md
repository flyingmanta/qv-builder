# QV Builder
A tool for building QlikView documents from project files

## Requirements
Node.js version >= 16.0.0

## Quick start
1. Initialize project
```bash
npm init
```
2. Install qv-builder
```bash
npm i "https://gitlab.com/flyingmanta/qv-builder"
```
3. Initialize project configuration
```bash
npx qv-builder init
```
4. Adjust environment variable values in .env file if necessary
5. Put QlikView project files (prj folders) at the root directory
6. Adjust entries in qv-builder.json file if necessary
7. Build QlikView files (qvw)
```bash
npx qv-builder
```

## Environment variables
Store environment variables in .env file

    QV_PATH=C:/Program Files/QlikView/Qv.exe
    QVW_TEMPLATE_PATH=template.qvw
    BUILD_PATH=dist
    SHELL_PATH=C:/Program Files (x86)/Git/bin/bash.exe

## Config
Store build configuration in qv-builder.json file
```json
[
    {
        "name": "New QlikView Document.qvw",
        "sort": 400
    }
]
```
Sort convention:
1. extractor 1xx,
2. generator 2xx,
3. model 3xx,
4. application 4xx

## Scripts
### Initialize project configuration
Create .env, .gitignore and qv-builder.json files
#### Using npx
```bash
npx qv-builder init
```
#### Using npm
Add following script to package.json file
```json
"scripts": {
    "build": "qv-builder"
},
```
```bash
npm run build init
```
### Build QlikView documents from project files
#### Using npx
```bash
npx qv-builder
```
#### Using npm
Add following script to package.json file
```json
"scripts": {
    "build": "qv-builder"
},
```
```bash
npm run build
```
