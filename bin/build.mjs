#!/usr/bin/env node

import 'zx/globals';
import 'dotenv/config';
import {getPrjs, getTmps, prepareBuildFolder} from '../src/fs.mjs';
import {prjToDoc, sortDocs, buildQvw} from '../src/qv.mjs';


export default async () => {
    try {
        console.time('build time');

        const BUILD_PATH = process.env.BUILD_PATH || 'dist';
        const QVW_TEMPLATE_PATH = process.env.QVW_TEMPLATE_PATH || 'template.qvw';
        const QV_PATH = process.env.QV_PATH || 'C:/Program Files/QlikView/Qv.exe';

        await prepareBuildFolder(BUILD_PATH);
        const prjs = await getPrjs();
        const docs = prjs.map(prjToDoc);
        sortDocs(docs);

        for (let i = 0; i < docs.length; i++) {
            await buildQvw(docs[i].qvw, docs[i].prj, QV_PATH, QVW_TEMPLATE_PATH, BUILD_PATH);
        }

    } catch (error) {
        console.error(chalk.red(error));
    } finally {
        const tmps = await getTmps();
        for (let i = 0; i < tmps.length; i++) {
            await fs.remove(tmps[i]);
        }
        console.timeEnd('build time');
    }
}
