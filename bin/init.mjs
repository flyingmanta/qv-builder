#!/usr/bin/env node

import 'zx/globals';
import 'path';
import { fs } from 'zx';
import { dirname } from 'path';
import { fileURLToPath } from 'url';


export default async () => {
    try {
        const cwd = process.cwd();
        const __dirname = dirname(fileURLToPath(import.meta.url));

        const qvBuilderJsonData = await fs.readJSON(path.join(__dirname, '../src/default/default-qv-builder.json'));
        await fs.writeJSON(path.join(cwd, 'qv-builder.json'), qvBuilderJsonData, {spaces: 2});

        const envData = await fs.readFile(path.join(__dirname, '../src/default/default.env'));
        await fs.writeFile(path.join(cwd, '.env'), envData);

        const gitignoreData = await fs.readFile(path.join(__dirname, '../src/default/default.gitignore'));
        await fs.writeFile(path.join(cwd, '.gitignore'), gitignoreData);

    } catch (error) {
        console.error(chalk.red(error));
    }
}
