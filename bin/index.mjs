#!/usr/bin/env node

import { $, argv } from 'zx';
import 'zx/globals';
import build from './build.mjs';
import init from './init.mjs';

$.verbose = false;
$.shell = process.env.SHELL_PATH || $.shell;

if(argv._.includes('init')){
    await init();
} else {
    await build();
}
